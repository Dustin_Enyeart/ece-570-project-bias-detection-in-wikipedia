import spacy
from textblob import TextBlob

import torch
import torch.nn as nn
import torch.nn.functional as F


class Unified_Model(nn.Module):
    '''
    The model is broken into submodels so that they can be trained
    separtely. This way, data can be processed before training, which
    gives a large increase in speed.
    '''
    def __init__(self, word_embedder, sentiment_analyzer, bias_checker):
        super(Unified_Model, self).__init__()

        self.word_embedder = word_embedder
        self.sentiment_analyzer = sentiment_analyzer
        self.bias_checker = bias_checker

        return None

    def forward(self, x):
        embedding = self.word_embedder(x)
        sentiment = self.sentiment_analyzer(x)
        x = self.bias_checker((embedding, sentiment))
        return x


class Preprocessor:

    def __init__(self, word_embedder, sentiment_analyzer):

        self.word_embedder = word_embedder
        self.sentiment_analyzer = sentiment_analyzer

        return None

    def __call__(self, x):
        embedding = self.word_embedder(x)
        sentiment = self.sentiment_analyzer(x)
        return (embedding, sentiment)


class Embedder:

    def __init__(self):
        self.nlp = spacy.load('en_core_web_sm')

        self.pos_tags = ['VERB',
                         'AUX',
                         'ADV',
                         'ADJ',
                         'DET',
                         'NOUN',
                         'PRON',
                         'PROPN',
                         'CCONJ',
                         'SCONJ',
                         'ADP',
                         'PART',
                         'INTJ',
                         'PUNCT',
                         'NUM',
                         'SYM',
                         'X',]
        self.tags_to_idx = {}
        for i, pos_tag in enumerate(self.pos_tags):
            self.tags_to_idx.update({pos_tag:i})
        self.pos_len = len(self.pos_tags)

        #SpaCy embeds into a 96-dimensional space.
        self.word_embedding_len = 96 + len(self.pos_tags)

        return None

    def pos_to_one_hot(self, pos_tag:'str'):
        idx = self.tags_to_idx[pos_tag]
        embedding = torch.zeros((self.pos_len,))
        embedding[idx] = 1.
        return embedding

    def __call__(self, sentences):
        if type(sentences) == str:
            sentences = [sentences]

        embedded_sentences = []
        for sentence in sentences:
          processed_sentence = self.nlp(sentence)
          embedded_sentence = []
          for word in processed_sentence:
            pos = word.pos_
            if pos == 'SPACE':
              continue
            pos = self.pos_to_one_hot(pos)
            embedded_word = torch.Tensor(word.vector)
            word_with_pos = torch.cat((embedded_word, pos))
            embedded_sentence.append(word_with_pos)
          embedded_sentence = torch.stack(embedded_sentence)
          embedded_sentences.append(embedded_sentence)
        embedded_sentences = nn.utils.rnn.pad_sequence(embedded_sentences,
                                                       batch_first=False)

        return embedded_sentences


class Sentiment_Analyzer:

    def __init__(self):
        self.sentiment_len = 2 #The length of the sentiment in TextBlob is 2.
        return None

    def __call__(self, sentences):
        if type(sentences) != list and type(sentences) != tuple:
            sentences = [sentences]

        sentiments = []
        for sentence in sentences:
            sentiment = TextBlob(sentence).sentiment
            sentiment = torch.Tensor(sentiment)
            sentiments.append(sentiment)
        sentiments = torch.stack(sentiments)

        return sentiments


class Bias_Checker(nn.Module):

    def __init__(self, word_embedding_len, sentiment_len,
                 hidden_size=8, num_layers=2, bidirectional=True,
                 dim_feedforward=8, nhead=4):
        super(Bias_Checker, self).__init__()

        self.word_embedding_len = word_embedding_len
        self.sentiment_len = sentiment_len

        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.bidirectional = bidirectional
        if self.bidirectional == True:
            self.num_directions = 2
        else:
            self.num_directions = 1
        self.gru = nn.GRU(input_size=self.word_embedding_len,
                          hidden_size=self.hidden_size,
                          num_layers=self.num_layers,
                          bidirectional=self.bidirectional,
                          dropout=.1)

        self.att = nn.Transformer(d_model=self.num_directions*self.hidden_size,
                                  nhead=nhead,
                                  dim_feedforward=dim_feedforward)

        self.fc1_in_dim = (self.num_directions*self.hidden_size
                           + self.sentiment_len)
        self.fc2_in_dim = 8
        self.fc1 = nn.Linear(self.fc1_in_dim, self.fc2_in_dim)
        self.fc2 = nn.Linear(self.fc2_in_dim, 1)

        return None

    def forward(self, x):
        sentences, sentiments = x
        #The shape of sentences is length×batch_size×word_embedding_len.
        #The shape of sentiments is batch_size×sentiment_len.

        h0 = torch.rand(self.num_directions*self.num_layers,
                        sentiments.shape[0], #The batch size
                        self.hidden_size)
        gru_out, _ = self.gru(sentences, h0)
        #The shape of gru_out is
        #length×batch_size×(num_directions*hidden_size).

        att_out = self.att(gru_out, gru_out)
        #The shape of att_out is the same shape as gru_out.

        x = att_out.mean(dim=0)
        #x = att_out.max(dim=0)[0]
        #The shape of x is batch_size×(num_directions×hidden_size).
        x = torch.cat((x, sentiments), dim=1)
        #The shape of x is
        #batch_size×(num_direction*hidden_size + sentiment_len).

        x = F.relu(self.fc1(x)) #The shape of x is batch_size×fc1_in_dim.
        x = torch.sigmoid(self.fc2(x)) #The shape of x is batch_size×1.
        return x
